﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestMakerFreeWebApp
{
    public class MySettings
    {
        public string ApplicationName { get; set; } = "My Great Application";
        public int MaxItemsPerList { get; set; } = 15;
    }
}
